;;(add-hook 'python-mode-hook '(lambda ()
  ;;  (local-set-key (kbd "RET") 'newline-and-indent)))


;;(setq-default fill-column 80)
;;(setq-default indent-tabs-mode nil)
;;(setq-default tab-width 4)
;;(setq indent-line-function 'insert-tab)
;;(setq-default transient-mark-mode t)

;;(defun set-newline-and-indent ()
;;  (local-set-key (kbd "RET") 'newline-and-indent))
;;(add-hook 'c-mode-hook 'set-newline-and-indent)
;;(add-hook 'c++-mode-hook 'set-newline-and-indent)
;;(add-hook 'html-mode-hook 'set-newline-and-indent)

;;(autoload 'js2-mode "js2-mode" nil t)
;;(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
;;(autoload 'espresso-mode "espresso")


;; Set the default c style to the k&r style
(setq c-default-style "k&r"
      c-basic-offset 4)

;; Automatic indentation so you don't have to hit tab on every new line
(require 'cc-mode)
(define-key c-mode-base-map (kbd "RET") 'newline-and-indent)

;; Set the font to 12
(set-face-attribute 'default nil :height 120)


;; Delete selected text when you press DEL, Ctrl-d or Backspace
;; Also, when you insert text while some text is selected, the selected
;; text will be deleted.
(delete-selection-mode t)
